/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * vlock, a very simple screen locker for Windows
 * Copyright (c) 2023 Vendicated
 */

#pragma once

#ifdef __cplusplus
#define DLL_EXPORT extern "C" __declspec(dllexport)
#else
#define DLL_EXPORT
#include <stdbool.h>
#endif

DLL_EXPORT bool DisableTskMan();
DLL_EXPORT bool RestoreTskMan();
