using System;
using System.Text;
using System.Runtime.InteropServices;

namespace vlock
{
    public static class Vlock
    {
        private static readonly String Password = "csharp";

        private static Native.KHConfig config;

        public static void Init()
        {
            config = new Native.KHConfig()
            {
                ClearCountsAsFail = true,
                RequireEnter = true,
                PwLength = Password.Length,
                Pw = Password,
                OnFail = (pw) =>
                {
                    Console.WriteLine("Failed to unlock with password: " + pw);
                },
                OnSuccess = () =>
                {
                    Environment.Exit(0);
                },
                OnInput = (inp, size, fullInp) =>
                {
                    if (size > 0 && inp[0] == 'q')
                    {
                        Console.WriteLine("Exiting...");
                        Environment.Exit(0);
                    }
                    Console.WriteLine(fullInp);
                }
            };

            Native.SetKHookConfig(config);
            if (!Native.InstallKHook())
            {
                Console.WriteLine("Failed to install keyboard hook");
                Environment.Exit(1);
            };
        }
    }

    static class Native
    {
        [DllImport("..\\..\\build\\vlock.dll", CharSet = CharSet.Unicode)]
        public static extern bool InstallKHook();

        [DllImport("..\\..\\build\\vlock.dll", CharSet = CharSet.Unicode)]
        public static extern void SetKHookConfig(KHConfig config);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct KHConfig
        {
            [MarshalAs(UnmanagedType.U1)]
            public bool ClearCountsAsFail;
            [MarshalAs(UnmanagedType.U1)]
            public bool RequireEnter;
            public int PwLength;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string Pw;
            public OnFailDelegate OnFail;
            public OnSuccessDelegate OnSuccess;
            public OnInputDelegate OnInput;

            [UnmanagedFunctionPointer(CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
            public delegate void OnFailDelegate([MarshalAs(UnmanagedType.LPWStr)] string pw);

            [UnmanagedFunctionPointer(CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
            public delegate void OnSuccessDelegate();

            [UnmanagedFunctionPointer(CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
            public delegate void OnInputDelegate(char[] inp, int size, [MarshalAs(UnmanagedType.LPWStr)] string fullInp);
        }
    }
}
