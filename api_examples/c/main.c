#ifndef UNICODE
#define UNICODE
#endif

#include <stdbool.h>
#include <stdio.h>
#include <windows.h>

#include "include/key_hook.h"
#include "include/taskmgr.h"

void onFail(const wchar_t* pw) {
    printf("%ls is not the correct password!\n", pw);
}

void onSuccess() { PostQuitMessage(0); }

void onInput(WCHAR buf[], int size, const wchar_t* input) {
    printf("Full Input: %ls\n", input);

    if (size > 0 && buf[0] == 'q') {
        PostQuitMessage(0);
    }
}

int WINAPI wWinMain(HINSTANCE instance, HINSTANCE prevInstance, PWSTR args,
                    int flag) {
    struct KHConfig config = {.ClearCountsAsFail = true,
                              .RequireEnter = true,
                              .Pw = L"password",
                              .PwLength = 8,
                              .OnFail = onFail,
                              .OnInput = onInput,
                              .OnSuccess = onSuccess};

    SetKHookConfig(config);
    if (!InstallKHook()) {
        // handle
    }
    if (!DisableTskMan()) {
        // handle
    }

    LPWSTR CLASS_NAME = L"VLOCK";

    WNDCLASS wc;
    wc.lpfnWndProc = DefWindowProc;
    wc.hInstance = instance;
    wc.lpszClassName = CLASS_NAME;

    RegisterClass(&wc);

    HWND win = CreateWindowEx(0, CLASS_NAME, L"vlock",
                              WS_CLIPSIBLINGS | WS_CLIPCHILDREN, CW_USEDEFAULT,
                              CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL,
                              NULL, instance, NULL);

    ShowWindow(win, SW_NORMAL);
    SetWindowPos(win, 0, 0, 0, 0, 0, 0);

    MSG msg;
    while (GetMessage(&msg, NULL, 0, 0) > 0) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    if (!RestoreTskMan()) {
        // handle
    }
    if (!UninstallKHook()) {
        // handle
    }

    return 0;
}
